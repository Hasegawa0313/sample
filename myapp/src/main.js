import { createApp } from "vue";
import App from "./App.vue";
import Teleport from "./teleport.vue";
import "./index.css";
import router from "./route.js";

const app = createApp(App);
const teleport = createApp(Teleport);

app.use(router);
app.mount("#app");

teleport.use(router);
teleport.mount("#teleport");
