import { createRouter, createWebHistory, createWebHashHistory } from "vue-router";
import Page_1 from "./App.vue";
import Page_2 from "./teleport.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", component: Page_1 },
    { path: "/teleport", component: Page_2 },
  ],
});

export default router;
